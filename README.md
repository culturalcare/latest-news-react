# latest-news-react

### Requirements
* React application using Redux
* Retrieve news items through API
* Reuse components as much as possible
* Adapts layout to screen size (responsive)
* Implements the layout found in `assets/layout.pdf`
* Unit tests

### Homepage has
* Image, date, title and description would be shown for each news item the first news item is render in hero mode
* Clicking on a news item tile will open details page

### Details Page has
* Reduced header
* Full width image background
* Title, description, date and a placeholder text body.

### Common elements
* The header would contain page title and home link, visible in all the pages
* The footer appears in different color, with Powered by NewsApi.org text link to �https://newsapi.org� and Top link, that will return to the top of the page

### Nice to have
* An extra button in the navigation bar, which will open a search input, where user can search news items
* "Load More" link after the first seven articles would load more articles at the bottom of the page
* Minimize the API requests

#### GET articles url
`https://newsapi.org/v2/everything?sources=bbc-news&apiKey=d4a2c9a8e6fa44949b4e7eedb705e5f8`

#### Extra parameters
* **pageSize**: control the number of news retrived. 20 is the default, 100 is the maximum.
* **q**: Keywords or phrases to search for.   
`https://newsapi.org/v2/everything?sources=bbc-news&q=world%20cup&apiKey=d4a2c9a8e6fa44949b4e7eedb705e5f8`  
* **sortBy=publishedAt**: newest articles come first
